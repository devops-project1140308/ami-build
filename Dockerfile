# Use Alpine Linux as the base image
FROM alpine:3.19.1

# Define build-time argument for HTTPD_PORT
ARG HTTPD_PORT

# Update package index and install required dependencies
RUN apk update && \
    apk add --no-cache ca-certificates wget unzip && \
    rm -rf /var/cache/apk/*
    
RUN apk add --no-cache git

# Install Ansible
RUN apk add --no-cache ansible

# Copy playbook to the container
COPY src/config/playbook.yml /playbook.yml

# Use the HTTPD_PORT argument to replace the placeholder in the playbook
RUN if [ -n "${HTTPD_PORT}" ]; then \
        sed -i "s/{{ httpd_port }}/${HTTPD_PORT}/g" /playbook.yml; \
    fi

# Run the playbook
CMD ["ansible-playbook", "/playbook.yml"]
